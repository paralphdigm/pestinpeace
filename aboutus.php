<section id="ourteam" class="">
  <div class="ourteam-content">
    <div class="">

      <div class="container">
        <div class="row">
          <div class="col-md-4">

            <div class="logo">
              <img src="img/logo-1.png" alt="" class="img-circle uk-align-center " width="80%">
            </div>
          </div>
          <div class="col-md-8">
            <div class="col-md-12">
              <h3 class="aqua-darken-text">About Us</h3>
              <p class="black-text">
              Pest-In-Peace Control Solutions is a licensed Pest Control Company, serving residential, commercial and industrial areas in Saskatoon and nearby areas for more than 2 years now. We specialize in Pest Management Services and our main priority is to keep our community happy by protecting families and properties from pests that are invading spaces around us. </p>
            </div>
            <div class="col-md-12">
              <h3 class="aqua-darken-text">Mission Statement</h3>
              <p class="black-text">
                Our mission is to provide peace of mind to Saskatoon home and building owners by protecting and preserving health and property through effective pest control services.
              </p>
              <p class="black-text">
                Pest-In-Peace Control Solutions is committed to providing our clients with an excellent experience.
                </p>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
