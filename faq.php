<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="css/main.css" media="screen" title="no title" charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="plugins/uikit/css/uikit.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="plugins/owl/owl.carousel.css">
    <link rel="stylesheet" href="plugins/owl/owl.theme.css">
    <link rel="stylesheet" href="plugins/owl/owl.transitions.css">
    <title>Pest in Peace</title>
  </head>
    <body>
      <header>
        <nav class="navbar navbar-fixed-top">
          <div class="navbar-pestinpeace top-menu">
              <a href="index" class="navbar-pestinpeace-brand">
                <img src="img/logo.png" />
              </a>
              <div class="navbar-pestinpeace-right hidden-xs hidden-sm" id="navbar-menu">
                <a href="index" class="navbar-pestinpeace-item navbar-pestinpeace-link">Home</a>
                <a href="index#services" class="navbar-pestinpeace-item navbar-pestinpeace-link">Services</a>
                <a href="index#ourteam" href="" class="navbar-pestinpeace-item navbar-pestinpeace-link">About</a>
                <a href="index#commonpests" class="navbar-pestinpeace-item navbar-pestinpeace-link">Common Pests</a>
                <a href="index#products" class="navbar-pestinpeace-item navbar-pestinpeace-link">Products</a>
                <a href="index#whychoose" class="navbar-pestinpeace-item navbar-pestinpeace-link">Facts</a>
                <a href="index#contactus" class="aqua white-text btn">Contact Us</a>
              </div>
              <div class="navbar-pestinpeace-item hidden-md hidden-lg" style="z-index: 9999;">
                <div class="dropdown">
                  <i class="fa fa-bars dropdown-toggle" data-toggle="dropdown"></i>
                  <ul class="dropdown-menu dropdown-menu-right navbar-pestinpeace-dropdown-menu">
                    <a href="index" class="btn white-text" uk-totop uk-scroll class="navbar-pestinpeace-item navbar-pestinpeace-link">Home</a>
                    <a href="index#services" class="navbar-pestinpeace-item navbar-pestinpeace-link">Services</a>
                    <a href="index#ourteam" href="" class="navbar-pestinpeace-item navbar-pestinpeace-link">About</a>
                    <a href="index#commonpests" class="navbar-pestinpeace-item navbar-pestinpeace-link">CommonPests</a>
                    <a href="index#products" class="navbar-pestinpeace-item navbar-pestinpeace-link">Products</a>
                    <a href="index#whychoose"  class="navbar-pestinpeace-item navbar-pestinpeace-link">Facts</a>
                    <a href="index#contactus"  class="navbar-pestinpeace-item navbar-pestinpeace-link">ContactUs</a>
                  </ul>
                </div>
              </div>
          </div>
        </nav>
      </header>
      <main>
        <div  class="light-gray-darken">
          <div class="container">
            <div style="margin-top: 13vh;">
              <div class="col-md-5">
                <div class="panel panel-default uk-padding">
                  <h3 class="aqua-darken-text">Pest Control Prevention Tips</h3>
                  <ul class="uk-list uk-list-bullet">
                    <li>Remove sources of food, water and shelter.</li>
                    <li>Store food in sealed plastic or glass containers. Garbage containing food scraps should be placed in tightly covered trash cans. Remove garbage regularly from your home.</li>
                    <li>Fix leaky plumbing and don't let water accumulate anywhere in the home. Don't let water collect in trays under your house plants or refrigerator. Don't leave pet food and water out overnight.</li>
                    <li>Clutter provides places for pests to breed and hide and makes it hard to get rid of them. Get rid of things like stacks of newspapers, magazines, or cardboard.</li>
                    <li>Close off places where pests can enter and hide. For example, caulk cracks and crevices around cabinets or baseboards. Use steel wool to fill spaces around pipes. Cover any holes with wire mesh.</li>
                    <li>Learn about the pests you have and options to control them.</li>
                    <li>Check for pests in packages or boxes before carrying them into your home.</li>
                  </ul>
                </div>
              </div>

              <div class="col-md-7">
                <div class="panel panel-default uk-padding">
                  <div class="row">
                    <h3 class="aqua-darken-text">Frequently asked Questions</h3>
                  </div>

                  <ul class="uk-list uk-list-divider">
                    <li>
                      <p class="aqua-darken-text"><b>Do I need to be in to receive a pest control treatment?</b></p>
                      <p>Yes, even if we treat outside, you will need to be on hand to listen to the advice provided by the pest control operative and to sign a disclaimer.</p>
                    </li>
                    <li>
                      <p class="aqua-darken-text"><b>How long do I have to leave the house for following a fumigation?</b></p>
                      <p>The pest control operative will advise you when you can return to an area following treatment. It is usually safe to do so after a period of around three hours.</p>
                    </li>
                    <li>
                      <p class="aqua-darken-text"><b>Are my pets safe?</b></p>
                      <p>As above, the pest control operative will explain the best method of treatment and when it is safe for your pests to return.</p>
                    </li>
                    <li>
                      <p class="aqua-darken-text"><b>What is involved in a treatment for rats or mice?</b></p>
                      <p>During your appointment, our team will talk to you to help establish the type of pest (if you aren’t certain) and where you saw or heard them. They will then survey the affected areas to find the cause of the infestation.</p>
                      <p>After discussing your personal needs, including whether you have small children or pets, they will recommend the best possible treatment to safely, effectively and humanely control the rats or mice.</p>
                      <p>Our pest controllers will also offer advice as to how you can reduce future issues with pests, including any preventative or pest proofing measures.</p>
                    </li>
                    <li>
                      <p class="aqua-darken-text"><b>Price range of extermination services. </b></p>
                      <p>Each extermination service will range in price depending on the size that needs to be treated. Our prices will be competitive with small competitors and a bit lower than big companies.  </p>
                    </li>
                    <li>
                      <p class="aqua-darken-text"><b>Are pest control methods toxic?</b></p>
                      <p>The team provides expert advice regarding the control of pests, as well as pest prevention methods and non- toxic treatments. Preventative pre-planned visits and annual contracts are available to businesses as well as scheduled appointments. We operate to the highest industry standards and offer safe and (where possible) environmentally friendly control.</p>
                    </li>
                  </ul>

                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      <footer style="border-top: 1px solid #ccc !important;">
        <div class="footer white" id="footer">
            <div class="container">
                <div class="row">

                    <div class="col-md-4">
                        <h3> Pest-In-Peace Control Solutions </h3>
                        <ul class="uk-list uk-list-divider">
                            <li>Commercial Pest Control Liability Insurance </li>
                            <li>Licensed Pesticide Service Corporation by the Government of Saskatchewan </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3> Contacts </h3>
                        <ul class="uk-list uk-list-divider">
                            <li> Phone: 306 716-9088</li>
                            <li> Email: pest-in-peace@outlook.ca</li>
                            <li> Saskatoon, Saskatchewan</li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3> Follow us on our Social Media  </h3>

                        <ul class="social">
                            <li class="aqua"> <a href="#"> <i class=" fa fa-facebook">   </i> </a> </li>
                            <li class="aqua"> <a href="#"> <i class="fa fa-twitter">   </i> </a> </li>
                            <li class="aqua"> <a href="#"> <i class="fa fa-google-plus">   </i> </a> </li>
                            <li class="aqua"> <a href="#"> <i class="fa fa-pinterest">   </i> </a> </li>
                            <li class="aqua"> <a href="#"> <i class="fa fa-youtube">   </i> </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom aqua-darken">
            <div class="container">
                <p class="pull-left white-text"> Copyright © Pest in Peace Control Slutions. </p>
                <div class="pull-right">
                  <a href="#" class="btn white-text" uk-totop uk-scroll></a>
                </div>
            </div>
        </div>
    </footer>
    </main>
  </body>
  <script type="text/javascript" src="plugins/jQuery/jquery-3.1.1.js"></script>
  <script type="text/javascript" src="plugins/owl/owl.carousel.min.js"></script>
  <script type="text/javascript" src="plugins/uikit/js/uikit.min.js"></script>
  <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
          $(".filter-button").click(function(){
               var value = $(this).attr('data-filter');
               if(value == "all")
               {
                   $('.filter').show('1000');
               }
               else
               {
                   $(".filter").not('.'+value).hide('3000');
                   $('.filter').filter('.'+value).show('3000');
               }
           });
           if ($(".filter-button").removeClass("active")) {
         $(this).removeClass("active");
         }
         $(this).addClass("active");

        $(".ourteam-carousel").owlCarousel({
          loop:true,
          autoPlay:true,
          margin:10,
          items:1,
          pagination: true,
          responsiveRefreshRate : 200,
          navigation:false,
          navigationText: [
           "<span class=' glyphicon glyphicon-chevron-left' aria-hidden='true'></span>",
           "<span class=' glyphicon glyphicon-chevron-right' aria-hidden='true'></span>"
            ]
        });
        $('.owl-carousel').owlCarousel({
            loop:true,
            autoPlay:true,
            margin:10,
            items:5,
            pagination: true,
            responsiveRefreshRate : 200,
            navigation:false,
            navigationText: [
             "<span class=' glyphicon glyphicon-chevron-left' aria-hidden='true'></span>",
             "<span class=' glyphicon glyphicon-chevron-right' aria-hidden='true'></span>"
              ]
        });
      });
  </script>
</html>
