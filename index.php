<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="css/main.css" media="screen" title="no title" charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="plugins/uikit/css/uikit.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="plugins/owl/owl.carousel.css">
    <link rel="stylesheet" href="plugins/owl/owl.theme.css">
    <link rel="stylesheet" href="plugins/owl/owl.transitions.css">
    <title>Pest in Peace</title>
  </head>
  <body>
    <header>

      <nav class="navbar navbar-fixed-top">
        <div class="templatemo-top-bar" id="templatemo-top" style="background-color: #027db1 !important; z-index: 2">
          <div class="container">
              <div class="subheader">
                  <div id="phone" class="pull-left white-text">
                          <img src="img/phone.png" alt="phone"/>
                          24/7 Support! - 306 716-9088
                  </div>
                  <div id="email" class="pull-right white-text">
                          <img src="img/email.png" alt="email"/>
                          pest-in-peace@outlook.ca
                  </div>
              </div>
          </div>
      </div>
        <div class="navbar-pestinpeace top-menu">

            <a href="#" class="navbar-pestinpeace-brand">
              <img src="img/logo.png" />
            </a>
            <!-- Right Navigation -->
            <div class="navbar-pestinpeace-right hidden-xs hidden-sm" id="navbar-menu">
              <a href="/" uk-scroll class="navbar-pestinpeace-item navbar-pestinpeace-link">Home</a>

              <div class="uk-inline">
                  <a class="navbar-pestinpeace-item navbar-pestinpeace-link">Services</a>
                  <div uk-dropdown="pos: bottom-justify">
                      <ul class="uk-nav uk-dropdown-nav">
                          <li><a href="#services" uk-scroll class="navbar-pestinpeace-item navbar-pestinpeace-link">Our Services</a></li>
                          <li><a href="faq" class="navbar-pestinpeace-item navbar-pestinpeace-link">FAQ</a></li>
                      </ul>
                  </div>
              </div>


              <a href="#ourteam" uk-scroll class="navbar-pestinpeace-item navbar-pestinpeace-link">About</a>
              <a href="#commonpests" uk-scroll class="navbar-pestinpeace-item navbar-pestinpeace-link">Common Pests</a>
              <a href="#products" uk-scroll class="navbar-pestinpeace-item navbar-pestinpeace-link">Products</a>
              <a href="#whychoose" uk-scroll class="navbar-pestinpeace-item navbar-pestinpeace-link">Facts</a>
              <a href="#contactus" uk-scroll class="aqua white-text btn">Contact Us</a>

            </div>

            <!-- Dropdown appearing on mobile only -->
            <div class="navbar-pestinpeace-item hidden-md hidden-lg" style="z-index: 9999;">
              <div class="dropdown">
                <i class="fa fa-bars dropdown-toggle" data-toggle="dropdown"></i>
                <ul class="dropdown-menu dropdown-menu-right navbar-pestinpeace-dropdown-menu">
                  <a href="#" class="btn white-text" uk-totop uk-scroll class="navbar-pestinpeace-item navbar-pestinpeace-link">Home</a>
                  <a href="#services" uk-scroll class="navbar-pestinpeace-item navbar-pestinpeace-link">Services</a>
                  <a href="#ourteam" uk-scroll href="" class="navbar-pestinpeace-item navbar-pestinpeace-link">About</a>
                  <a href="#commonpests" uk-scroll class="navbar-pestinpeace-item navbar-pestinpeace-link">CommonPests</a>
                  <a href="#products" uk-scroll class="navbar-pestinpeace-item navbar-pestinpeace-link">Products</a>
                  <a href="#whychoose" uk-scroll class="navbar-pestinpeace-item navbar-pestinpeace-link">Facts</a>
                  <a href="#contactus" uk-scroll class="navbar-pestinpeace-item navbar-pestinpeace-link">ContactUs</a>
                </ul>
              </div>
            </div>


        </div>
      </nav>
      <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
        <!-- Overlay -->

        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
          <li data-target="#bs-carousel" data-slide-to="1"></li>
          <li data-target="#bs-carousel" data-slide-to="2"></li>
          <li data-target="#bs-carousel" data-slide-to="3"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <div class="item slides active">
            <div class="slide-1"></div>
            <div class="hero">
              <hgroup>
                  <h1 class="aqua-darken-text">Pest in Peace</h1>
                  <h2 class="aqua-text">Control Solutions</h2>
                  <h4 class="light-gray-text">
                    It's time to exterminate!  We offer professional pest control solutions to Saskatoon surrounding areas.
                  </h4>
              </hgroup>
            </div>
          </div>
          <div class="item slides">
            <div class="slide-2"></div>
            <div class="hero">
              <hgroup>
                  <h1 class="white-text">Guaranteed returned service</h1>
                  <h4 class="light-gray-text">We will guarantee our technicians' work by offering return service if the customer is not satisfied with the treatments. This applies to all customers with service agreements. </h4>
              </hgroup>
            </div>
          </div>
          <div class="item slides">
            <div class="slide-3"></div>
            <div class="hero">
              <hgroup>
                <h1 class="white-text">Response time</h1>
                <h4 class="light-gray-text">The company will send a technician within 24 hours.</h4>
              </hgroup>
            </div>
          </div>
          <div class="item slides">
            <div class="slide-4"></div>
            <div class="hero">
              <hgroup>
                <h1 class="white-text">Instant quote</h1>
                <h4 class="light-gray-text">After an inspection, a technician will provide a quote for treatment, which can help you stay within your budget, even in an emergency situation.</h4>
              </hgroup>
            </div>
          </div>
        </div>
      </div>
    </header>

    <main>
      <?php include('services.php'); ?>

      <?php include('aboutus.php'); ?>

      <?php include('commonpests.php'); ?>


      <section id="products" class="section white text-center">
        <div class="container">
          <div class="row section-header">
            <div class="col-md-offset-2 col-md-8">
              <h2 class="aqua-text">Our Products</h2>
              <p class="aqua-darken-text">It’s time to exterminate!</p>
            </div>
          </div>
          <div class="row">
            <div class="owl-carousel owl-theme">
              <div class="item">
                <div class="our-services uk-padding-small">
                  <img src=" img/icons/bug.png" alt="" class="img-circle  " style="width: 50%;">
                  <h4>Bait stations, rodent control</h4>
                  </div>
              </div>

              <div class="item">
                <div class="our-services uk-padding-small">
                  <img src=" img/icons/bug.png" alt="" class="img-circle  " style="width: 50%;">
                  <h4>Glue boards for insects and rodent control</h4>
                  </div>
              </div>

              <div class="item">
                <div class="our-services uk-padding-small">
                  <img src=" img/icons/bug.png" alt="" class="img-circle  " style="width: 50%;">
                  <h4>Insects Fly lights</h4>
                  </div>
              </div>

              <div class="item">
                <div class="our-services uk-padding-small">
                  <img src=" img/icons/bug.png" alt="" class="img-circle  " style="width: 50%;">
                  <h4>Poison corn for pigeon</h4>
                  </div>
              </div>

              <div class="item">
                <div class="our-services uk-padding-small">
                  <img src=" img/icons/bug.png" alt="" class="img-circle " style="width: 50%;">
                  <h4>Trays for birds control</h4>
                  </div>
              </div>

              <div class="item">
                <div class="our-services uk-padding-small">
                  <img src=" img/icons/bug.png" alt="" class="img-circle " style="width: 50%;">
                  <h4>Domestic insecticides</h4>
                  </div>
              </div>

              <div class="item">
                <div class="our-services uk-padding-small">
                  <img src=" img/icons/bug.png" alt="" class="img-circle" style="width: 50%;">
                  <h4>Rodent devices</h4>
                  </div>
              </div>

              <div class="item">
                <div class="our-services uk-padding-small">
                  <img src=" img/icons/bug.png" alt="" class="img-circle" style="width: 50%;">
                  <h4>Wild life live traps</h4>
                  </div>
              </div>

            </div>
          </div>

        </div>
      </section>

      <section id="whychoose" class="">
        <div class="whychoose-content">
          <div class="">
            <div class="row section-header text-center">
              <div class="col-md-offset-2 col-md-8">
                <h2 class="white-text">WHY CHOOSE PEST IN PEACE CONTROL SOLUTION?</h2>

              </div>
            </div>
            <div class="container">
              <div class="row">
                <div class="col-md-6">
                  <h3 class="white-text">Facts and Fiction</h3>
                  <div class="">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/n1GhM6H1tRg" frameborder="0" allowfullscreen></iframe>
                  </div>
                  <br><br>
                </div>
                <div class="col-md-6">

                  <div class="col-md-12">
                    <h4 class="white-text">Residential, Commercial & Industrial Pest Control Services</h4>
                    <p class="light-gray-text">
                    Pest in domestic homes, commercial properties and industrial areas are not only annoying but they can also cause structural damage and health risks to humans.<br>
                    </p>
                    <p><a class="btn aqua white-text" href="#residential" uk-toggle>Read More</a></p>

                    <div id="residential" uk-modal="center: true" style="z-index: 99999;">
                        <div class="uk-modal-dialog">
                            <button class="uk-modal-close-default" type="button" uk-close></button>
                            <div class="uk-modal-body">
                              <div class="row">
                                <div class="col-md-12">
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <h2>Residential, Commercial & Industrial Pest Control Services</h2>
                                  <p>
                                    Pest in domestic homes, commercial properties and industrial areas are not only annoying but they can also cause structural damage and health risks to humans.
                                    <br><br>
                                    Pest-in-Peace Control Solutions puts you and your family first with safe, quick and thorough pest treatment and prevention solutions. We take the time to understand your needs and determine the best solution. Our team will explain the process and guide you, every step of the way so you know exactly what to expect.
                                    <br><br>
                                    Our treatments and products that we use are pest specific and not harmful to humans or pets. Rest assured, knowing your pest concern will be treated with the highest quality products, in a safe and environmentally friendly method.
                                    <br><br>
                                    All treatments are carried out by friendly, well-trained, licensed, professional pest technicians. We always follow the Canada standards where applicable and use only the highest quality products to ensure your pest problem is treated thoroughly.
                                    <br><br>
                                    Our professional pest technicians are trained not only to treat the pest problem but also to look at the cause of your home pest problems. They will take the time to understand your concerns and provide feedback and recommendations on how to minimize pests in and around the home.
                                    <br><br>
                                    All our professionally trained and experienced technicians take care when carrying out treatment at your home and will work with your family to accommodate any of your pest control needs.
                                  </p>
                                  </div>
                              </div>

                            </div>
                        </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <h4 class="white-text">Inspections</h4>
                    <p class="light-gray-text">
                    We begin our relationship with each customer with a thorough inspection of the building or home so we can develop a customized pest control plan.<br>
                    </p>
                    <p><a class="btn aqua white-text" href="#inspections" uk-toggle>Read More</a></p>

                    <div id="inspections" uk-modal="center: true" style="z-index: 99999;">
                        <div class="uk-modal-dialog">
                            <button class="uk-modal-close-default" type="button" uk-close></button>
                            <div class="uk-modal-body">
                              <div class="row">
                                <div class="col-md-12">
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <h2>Inspections</h2>
                                  <p>We begin our relationship with each customer with a thorough inspection of the building or home so we can develop a customized pest control plan. The technician will provide a list of the areas treated, along with information about how long the treated areas need to be avoided. This type of detailed report is important, especially for those who are sensitive to common chemicals used in pest eradication.
                                  </p>
                                  </div>
                              </div>

                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <h4 class="white-text">Pest removal</h4>
                    <p class="light-gray-text">Prevention services includeblocking all possible entry points in the building exterior and interior, cutting off all source of feeding the.</p>
                    <p><a class="btn aqua white-text" href="#pest-removal" uk-toggle>Read More</a></p>

                    <div id="pest-removal" uk-modal="center: true" style="z-index: 99999;">
                        <div class="uk-modal-dialog">
                            <button class="uk-modal-close-default" type="button" uk-close></button>
                            <div class="uk-modal-body">
                              <div class="row">
                                <div class="col-md-12">
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <h2>Pest removal</h2>
                                  <p>Prevention services include blocking all possible entry points in the building exterior and interior, cutting off all source of feeding the pest could possible get the food that attract the pest.
                                  Elimination services involve spraying insecticide, baiting, traps, pesticides, rodenticides and more.</p>

                                  </div>
                              </div>

                            </div>
                        </div>
                    </div>


                  </div>
                </div>

              </div>

            </div>
          </div>
        </div>
      </section>

      <section id="contactus" class="mapwrapper" style="height: 100vh; max-width: 100%; position: relative; border-top: 2px solid #ccc; border-bottom: 2px solid #ccc; ">
        <div id='map' style="width: 100%;height: 100vh; "></div>
        <div class="mapoverlay" style="position: absolute; z-index: 2; top: 0; width: 100%; height: 100%; background-image: linear-gradient(-225deg, rgba(255,255,255,0.1) 40%, rgba(0,101,168,0.3) 90%), url();">
          <div class="container">
            <div class="col-lg-5 col-lg-offset-8 col-md-6">
                  <div class="panel panel-default uk-padding aqua" style="margin-top: 5vh;">
                    <div class="row white-text">
                      <div class="col-md-12">
                        <h4 class="white-text">Get A Quote Today!</h4>
                        <p>Fill out this form and we will get back to you within 24 hours.</p>
                      </div>

                    </div>
                    <div class="row">
                      <form class="" role="form" method="post" action="PHPMailer/mailer.php" enctype="multipart/form-data">

                          <div class="form-group col-md-6">
                              <input type="text" name="name" class="uk-input input-text uk-form-small" id="name" placeholder="Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                              <div class="validation"></div>
                          </div>

                          <div class="form-group col-md-6">
                              <input type="email" name="email" class="uk-input input-text uk-form-small" id="email" placeholder="Email Address" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                              <div class="validation"></div>
                          </div>

                          <div class="form-group col-md-12">
                              <input type="text" name="phonenumber" class="uk-input input-text uk-form-small" id="phonenumber" placeholder="Phone Number" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                              <div class="validation"></div>
                          </div>

                          <div class="form-group col-md-12">
                              <input type="text" name="address" class="uk-input input-text uk-form-small" id="address" placeholder="Address" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                              <div class="validation"></div>
                          </div>


                          <div class="form-group col-md-12">
                            <select name="subject" class="uk-select" style="height: 30px; line-height: 28px;">
                                <option value="">Option 01</option>
                                <option value="">Option 02</option>
                            </select>
                          </div>
                          <!-- <div class="form-group">
                              <input type="text" name="subject" class="uk-input input-text uk-form-small" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                              <div class="validation"></div>
                          </div> -->

                          <div class="form-group col-md-12">
                              <textarea name="message" class="uk-textarea uk-form-small" rows="3" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                              <div class="validation"></div>
                          </div>
                          <div class="form-group col-md-12">
                            <input class="col-md-12 uk-button white uk-width-1-1 uk-margin-small-bottom" value="Send" type="submit">
                          </div>
                      </form>
                    </div>

                  </div>
              </div>
          </div>
        </div>
    </section>

  <footer>
    <div class="footer white" id="footer">
        <div class="container">
            <div class="row">

                <div class="col-md-4">
                    <h3> Pest-In-Peace Control Solutions </h3>
                    <ul>
                        <li> Saskatoon, Saskatchewan</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3> Contacts </h3>
                    <ul>
                        <li> Phone: 306 716-9088</li>
                        <li> Email: pest-in-peace@outlook.ca</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3> Follow us on our Social Media  </h3>

                    <ul class="social">
                        <li class="aqua"> <a href="https://www.facebook.com/pestinpeace2016/?ref=nf" target="blank"> <i class=" fa fa-facebook">   </i> </a> </li>
                        <li class="aqua"> <a href="https://www.instagram.com/pestinpeace306/?hl=en" target="blank"> <i class="fa fa-instagram">   </i> </a> </li>
                        <li class="aqua"> <a href="https://www.linkedin.com/in/pest-in-peace-control-solutions-141124127" target="blank"> <i class="fa fa-linkedin">   </i> </a> </li>
                        <li class="aqua"> <a href="https://plus.google.com/communities/114696174730574449642" target="blank"> <i class="fa fa-google-plus">   </i> </a> </li>
                        <li class="aqua"> <a href="https://www.youtube.com/channel/UCABOh0nTIivdRo5Q4iDqvEg" target="blank"> <i class="fa fa-youtube">   </i> </a> </li>
                    </ul>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </div>
    <!--/.footer-->

    <div class="footer-bottom aqua-darken">
        <div class="container">
            <p class="pull-left white-text"> Copyright © Pest in Peace Control Slutions. </p>
            <div class="pull-right">
              <a href="#" class="btn white-text" uk-totop uk-scroll></a>
            </div>
        </div>
    </div>
    <!--/.footer-bottom-->
</footer>
    </main>


  </body>
  <script type="text/javascript" src="plugins/jQuery/jquery-3.1.1.js"></script>
  <script type="text/javascript" src="plugins/owl/owl.carousel.min.js"></script>
  <script type="text/javascript" src="plugins/uikit/js/uikit.min.js"></script>
  <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.min.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWgMSeuS1T9FjUStz8Wy9-K5L7Nwpa4Qg&callback=initMap"
type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $("#modal-ants").appendTo("body");
});
function initMap() {
      // Styles a map in night mode.
      var myLatLng = {lat: 52.115603, lng: -106.5862807};
      var centerlatlng = {lat: 52.115903, lng: -106.5862807};

      var map = new google.maps.Map(document.getElementById('map'), {
        center: centerlatlng,
        zoom: 16,
        styles: [
          {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
      }
    ]
      });
      var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Hello World!'
      });

        var infowindow = new google.maps.InfoWindow();
        infowindow.setContent(
          '<div id="content">'+
               '<div id="siteNotice">'+'</div>'+
               '<div id="bodyContent">'+
                   '<h4 class="aqua-text">Pest in Peace Control Solutions</h4>'+
                   '<span><b>Address:</b> 114 Edinburgh place Saskatoon, Sk.</span><br><br>'+
                   '<span><b>Phone Number:</b> 306-7169088</span><br><br>'+
                   '<span><b>Email:</b>  pest-in-peace@outlook.com</span>'+
                   ''+
              '</div>'+
          '</div>');
        infowindow.open(map, marker);
    }

</script>
<script type="text/javascript">
  $(document).ready(function() {
        $(".filter-button").click(function(){
             var value = $(this).attr('data-filter');

             if(value == "all")
             {
                 //$('.filter').removeClass('hidden');
                 $('.filter').show('1000');
             }
             else
             {
       //            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
       //            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                 $(".filter").not('.'+value).hide('3000');
                 $('.filter').filter('.'+value).show('3000');

             }
         });

         if ($(".filter-button").removeClass("active")) {
       $(this).removeClass("active");
       }
       $(this).addClass("active");

      $(".ourteam-carousel").owlCarousel({
        loop:true,
        autoPlay:true,
        margin:10,
        items:1,
        pagination: true,
        responsiveRefreshRate : 200,
        navigation:false,
        navigationText: [
         "<span class=' glyphicon glyphicon-chevron-left' aria-hidden='true'></span>",
         "<span class=' glyphicon glyphicon-chevron-right' aria-hidden='true'></span>"
          ]
      });
      $('.owl-carousel').owlCarousel({
          loop:true,
          autoPlay:true,
          margin:10,
          items:5,
          pagination: true,
          responsiveRefreshRate : 200,
          navigation:false,
          navigationText: [
           "<span class=' glyphicon glyphicon-chevron-left' aria-hidden='true'></span>",
           "<span class=' glyphicon glyphicon-chevron-right' aria-hidden='true'></span>"
            ]
      });
    });

</script>

</html>
