<section id="services" class="section text-center">
  <div class="container">
    <div class="row section-header">
      <div class="col-md-offset-2 col-md-8">
        <h2 class="aqua-text">OUR CONTROL SERVICES</h2>
        <p class="text-center">Residential, Commercial & Industrial Pest Control Services</p>
        <!-- <p class="aqua-darken-text">ghsjdhgk shgksh dghs kghsk dghskdh gksj</p> -->
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="our-services uk-padding-small">
          <div class="row">
            <img class="col-md-offset-3 col-md-6 col-xs-4 col-xs-offset-4" src="img/icons/bug.png" alt="" class=""><br>
          </div>
          <div class="row">
            <h3>Got unwanted guests?</h3>
            <p>Pest-in-Peace offers custom pest control plans in Saskatoon. We specialize in removal and prevention of infestations by insects and rodents, including cockroaches, fleas, ticks, ants, bees, mice, and a host of other irritating invasive species. </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="our-services uk-padding-small">
          <div class="row">
            <img class="col-md-offset-3 col-md-6 col-xs-4 col-xs-offset-4" src="img/icons/licensed.png" alt="" class=""><br>
          </div>
          <div class="row">
            <h3>Licensed</h3>
            <p>All treatments are carried out by friendly, well-trained, licensed, professional pest technicians. We always follow the Canadian standards and use only the highest quality products to ensure pest problem is treated thoroughly.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="our-services uk-padding-small">
          <div class="row">
            <img class="col-md-offset-3 col-md-6 col-xs-4 col-xs-offset-4" src="img/icons/prof.png" alt="" class=""><br>
          </div>
          <div class="row">
            <h3>Professional Service</h3>
            <p>Our professional pest technicians are trained not only to treat the pest problem but also to look at the cause. They will take the time to understand concerns and provide feedback and recommendations on how to minimize pests in and around the building.</p>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-4">
        <div class="our-services uk-padding-small">
          <div class="row">
            <img class="col-md-offset-3 col-md-6 col-xs-4 col-xs-offset-4 col-xs-4 col-xs-offset-4" src="img/icons/guard.png" alt="" class=""><br>
          </div>
          <div class="row">
            <h3>Guarantee</h3>
            <p>All our professionally trained and experienced technicians take care when carrying out treatment and will work with the client to accommodate any needs. </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="our-services uk-padding-small">
          <div class="row">
            <img class="col-md-offset-3 col-md-6 col-xs-4 col-xs-offset-4" src="img/icons/eco.png" alt="" class=""><br>
          </div>
          <div class="row">
            <h3>Eco-Friendly</h3>
            <p>Our treatments and products are pest specific, safe for humans and pets and environmentally friendly.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="our-services uk-padding-small">
          <div class="row">
            <img class="col-md-offset-3 col-md-6 col-xs-4 col-xs-offset-4" src="img/icons/needhelp.png" alt="" class=""><br>
          </div>
          <div class="row">
            <h3>Need help?</h3>
            <p>We'll protect your home or business from unwanted guests. We’ll help you with any kind of pest services. We offer good prices good results residential and commercial.</p>
          </div>
        </div>
      </div>

    </div>
  </div>

</section>
