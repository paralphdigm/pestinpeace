<section id = "commonpests" class="section light-gray text-center">
  <div class="container">
    <div class="row">
    <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h2 class="aqua-text">COMMON PESTS</h2>
      <p class="aqua-darken-text">It’s time to exterminate!</p>
    </div>

    <div align="center">
        <button class="uk-button uk-button-primary uk-button-small filter-button" data-filter="all">All</button>
        <button class="uk-button uk-button-primary uk-button-small filter-button" data-filter="insects">INSECTS</button>
        <button class="uk-button uk-button-primary uk-button-small filter-button" data-filter="rodents">RODENTS</button>
        <button class="uk-button uk-button-primary uk-button-small filter-button" data-filter="wildlife">WILDLIFE</button>
    </div>
    <br/>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter insects">
            <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/ants.jpg');">
              <div class="card-category">INSECTS</div>
              <div class="card-description">
                <p class="text-left blue-darken-text"><strong>ANTS</strong></p>
                <p>Ants form colonies that range in size from a few dozen predatory individuals living..<br><strong class="aqua-text">ReadMore</strong></p>
              </div>
              <a class="card-link" href="#modal-ants" uk-toggle></a>

              <div id="modal-ants" uk-modal="center: true" style="z-index: 99999;">
                  <div class="uk-modal-dialog">
                      <button class="uk-modal-close-default" type="button" uk-close></button>
                      <div class="uk-modal-body">
                        <div class="row">
                          <div class="col-md-12">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <h2>ANTS</h2>
                            <p>Ants form colonies that range in size from a few dozen predatory individuals living in small natural cavities to highly organized colonies that may occupy large territories and consist of millions of individuals. Larger colonies consist mostly of sterile, wingless females forming castes of "workers", "soldiers", or other specialised groups. Nearly all ant colonies also have some fertile males called "drones" and one or more fertile females called "queens". The colonies are described as superorganisms because the ants appear to operate as a unified entity, collectively working together to support the colony.</p>

                            <ul class="uk-list uk-list-divider">
                              <li><b>Colour:</b> Light brown to black, sometimes multicoloured</li>
                              <li><b>Legs:</b> 6</li>
                              <li><b>Shape:</b> Long, segmented; heart-shaped abdomen</li>
                              <li><b>Size:</b> 1/16 – 1/8” (2.5 – 4 mm)</li>
                              <li><b>Antennae:</b> Yes</li>
                              <li><b>Region:</b> All</li>
                            </ul>
                            </div>
                        </div>

                      </div>
                  </div>
              </div>

            </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter rodents">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/bats.jpg');">
            <div class="card-category">RODENTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>BATS</strong></p>
              <p>Bats are a potentially dangerous infestation to control. Not only can they as pests..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-bats" uk-toggle></a>

            <div id="modal-bats" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>BATS</h2>
                          <p>Bats are a potentially dangerous infestation to control. Not only can they as pests cause problems, but there are medical implications to consider. Bats can carry rabies without showing any signs, and working near bats may increase chances of exposure.</p>

                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter insects">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/carpetbeetle.jpg');">
            <div class="card-category">INSECTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>CARPET BEETLE</strong></p>
              <p>Carpet beetles come in two basic types: black and varied. Black adults are 3..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-carpetbeetle" uk-toggle></a>
            <div id="modal-carpetbeetle" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Carpet Beetle</h2>
                          <p>Carpet beetles come in two basic types: black and varied. Black adults are 3 to 5 mm long and have dark brown or black bodies; varied adults are 2 to 3 mm long and have black bodies with irregular white, brown, orange or yellow scales. Carpet beetles are destructive during their larval stage, feeding on a wide variety of items including carpet, felt, wool, skins, furs, stuffed animals, leather, feathers, hair, dander and insect meal. They are most active in dark and undisturbed areas. Due to their food selection, regular vacuuming is one of the best ways to keep them from infesting your home.</p>
                          <ul class="uk-list uk-list-divider">
                            <li><b>Colour:</b> Gray or blackish body, wings usually lacking dark areas but some species with entirely dark wings; eyes often large and green or purple with horizontal stripes</li>
                            <li><b>Legs:</b> 6</li>
                            <li><b>Shape:</b> Stout-bodied and without bristles</li>
                            <li><b>Size:</b> About 3/8 - 1 and 1/8” (10-30 mm) long</li>
                            <li><b>Antennae:</b> Yes</li>
                            <li><b>Region:</b> Found throughout North America</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                </div>
            </div>

          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter insects">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/centipedes.jpg');">
            <div class="card-category">INSECTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>CENDIPEDES</strong></p>
              <p>Centipedes have a rounded or flattened head, bearing a pair of antennae at..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-centipede" uk-toggle></a>
            <div id="modal-centipede" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Carpet Beetle</h2>
                          <p>Centipedes have a rounded or flattened head, bearing a pair of antennae at the forward margin. They have a pair of elongated mandibles, and two pairs of maxillae. The first pair of maxillae form the lower lip, and bear short palps. The first pair of limbs stretch forward from the body to cover the remainder of the mouth. These limbs, or maxillipeds, end in sharp claws and include venom glands that help the animal to kill or paralyze its prey. </p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter insects">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/cockroaches.jpg');">
            <div class="card-category">INSECTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>COCKROACHES</strong></p>
              <p>Cockroaches are one of the most commonly noted household pest insects..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-cockroaches" uk-toggle></a>
            <div id="modal-cockroaches" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Cockroaches</h2>
                          <p>Cockroaches are one of the most commonly noted household pest insects. They feed on human and pet food and can leave an offensive odor. They can also passively transport microbes on their body surfaces including those that are potentially dangerous to humans, particularly in environments such as hospitals. Cockroaches are linked with allergic reactions in humans. One of the proteins that triggers allergic reactions is tropomyosin. These allergens are also linked with asthma.
                          General preventive measures against household pests include keeping all water and food stored away in sealed containers, using garbage cans with tight lids, frequent cleaning in the kitchen, and regular vacuuming. Any water leaks, such as dripping taps, should also be repaired. It is also helpful to seal off any entry points, such as holes around baseboards, between kitchen cabinets, pipes, doors, and windows with some steel wool or copper mesh and some cement, putty or silicone caulk.
                          </p>
                          <ul class="uk-list uk-list-divider">
                            <li><b>Colour:</b> Brown, with pronounced banding across wings</li>
                            <li><b>Legs:</b> 6</li>
                            <li><b>Shape:</b> Oval</li>
                            <li><b>Size:</b>1/2" long</li>
                            <li><b>Antennae:</b> Yes</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter insects">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.1), rgba(0,0,0,0.7)), url(' img/controlservice/flies.jpg');">
            <div class="card-category">INSECTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>FLIES</strong></p>
              <p>Fleas are small flightless insects that form the order Siphonaptera..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-flies" uk-toggle></a>
            <div id="modal-flies" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Flies</h2>
                          <p>Fleas are small flightless insects that form the order Siphonaptera. As external parasites of mammals and birds, they live by consuming the blood of their hosts. Adults are up to about 3 mm (0.12 in) long and usually brown. Bodies flattened sideways enable them to move through their host's fur or feathers; strong claws prevent them from being dislodged. They lack wings, and have mouthparts adapted for piercing skin and sucking blood and hind legs adapted for jumping. The latter enable them to leap a distance of some 50 times their body length, a feat second only to jumps made by froghoppers. Larvae are worm-like with no limbs; they have chewing mouthparts and feed on organic debris.
                          </p>

                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter rodents">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.1), rgba(0,0,0,0.7)), url(' img/controlservice/gophers.jpg');">
            <div class="card-category">RODENTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>GOPHERS</strong></p>
              <p>While gophers can be an occasional nuisance to gardeners and homeowne..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-gopher" uk-toggle></a>
            <div id="modal-gopher" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Gopher</h2>
                          <p>While gophers can be an occasional nuisance to gardeners and homeowners, they can cause extensive and significant damage to local ecosystems and agriculture. The amount of soil disturbed by a single population of gophers in one area is usually measured in tons due to the amount of damage they can cause.
                          </p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter insects">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/hornets.jpg');">
            <div class="card-category">INSECTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>HORNETS</strong></p>
              <p>Hornets (insects in the genera Vespa and Provespa) are the largest..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-hornets" uk-toggle></a>
            <div id="modal-hornets" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Hornets</h2>
                          <p>Hornets (insects in the genera Vespa and Provespa) are the largest of the eusocial wasps, and are similar in appearance to their close relatives yellowjackets. Some species can reach up to 5.5 cm (2.2 in) in length. They are distinguished from other vespine wasps by the relatively large top margin of the head and by the rounded segment of the abdomen just behind the waist. Worldwide, there are 22 recognized species of Vespa, and three species of Provespa, which are unique amongst hornets in being nocturnal. Wasps native to North America in the genus Dolichovespula are commonly referred to as hornets, but are actually yellowjackets.
                          </p>

                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter insects">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/maplebug.jpg');">
            <div class="card-category">INSECTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>MAPLEBUG</strong></p>
              <p>The boxelder bug or Maple bug (Boisea trivittata) is a North American..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-maplebug" uk-toggle></a>
            <div id="modal-maplebug" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Maplebug</h2>
                          <p>The boxelder bug or Maple bug (Boisea trivittata) is a North American species of true bug. It is found primarily on boxelder trees, as well as maple and ash trees. The adults are about 12.5 millimetres (0.49 in) long with a dark brown or black colouration, relieved by red wing veins and markings on the abdomen; nymphs are bright red.
                          </p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter insects">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/moles.jpg');">
            <div class="card-category">INSECTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>MOLES</strong></p>
              <p>Lawn damage caused by moles plagues homeowners and lawn..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-moles" uk-toggle></a>
            <div id="modal-moles" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Moles</h2>
                          <p>Lawn damage caused by moles plagues homeowners and lawn care specialists alike. Seldom seen as they tirelessly tunnel underground, moles leave their telltale marks aboveground as unsightly "mole hills", mounds of soil, or grassless brown streaks. Trapping is most successful during the spring and fall months after a rain. Moles are more difficult to locate in the summer and winter months, since their tunnels are deeper in the soil. When using a mole trap, locate the active runways first. Do this by stepping on a run or mound and mark the location. Wait for 24-48 hours to see if the opening is re-opened (indicating mole activity). Place mole traps in this location.
                          </p>

                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter insects">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/moth.jpg');">
            <div class="card-category">INSECTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>MOTH</strong></p>
              <p>Several moths in the family Tineidae are commonly regarded..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-moth" uk-toggle></a>
            <div id="modal-moth" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Moth</h2>
                          <p>Several moths in the family Tineidae are commonly regarded as pests because their larvae eat fabric such as clothes and blankets made from natural proteinaceous fibers such as wool or silk. They are less likely to eat mixed materials containing some artificial fibers. There are some reports that they may be repelled by the scent of wood from juniper and cedar, by lavender, or by other natural oils; however, many consider this unlikely to prevent infestation. Naphthalene (the chemical used in mothballs) is considered more effective, but there are concerns over its effects on human health.
                          </p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter rodents">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/ratmice.jpg');">
            <div class="card-category">RODENTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>Rat and Mice</strong></p>
              <p>Rats can serve as zoonotic vectors for certain pathogens..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-ratmice" uk-toggle></a>
            <div id="modal-ratmice" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Rats & Mice</h2>
                          <p>Rats can serve as zoonotic vectors for certain pathogens and thus spread disease, such as bubonic plague, Lassa fever, leptospirosis, and Hantavirus infection
Norway rats can cause damage to structures through their gnawing and eating. These rats are also vectors of diseases including plague, jaundice, rat-bite fever, cowpox virus, trichinosis and salmonellosis. In addition, Norway rats can contaminate food and introduce fleas into a home.

                          </p>
                          <ul class="uk-list uk-list-divider">
                            <li><b>Colour:</b> Brown with scattered black hairs; gray to white underside</li>
                            <li><b>Legs:</b> 4</li>
                            <li><b>Shape:</b> Long, heavily bodied; blunt muzzle</li>
                            <li><b>Size:</b>7-9 ½ inches long</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter wildlife">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/exclusion.jpg');">
            <div class="card-category">WILDLIFE</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>Exclusion</strong></p>
              <p>Exclusion is the humane and life-oriented approach to pest control..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-exclusion" uk-toggle></a>
            <div id="modal-exclusion" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Exclusion</h2>
                          <p>Exclusion is the humane and life-oriented approach to pest control. Techniques are applied to remove intrusive animals and prevent future use of your building or home by wildlife. Rather than trap and relocate animals, which is illegal and can expose wildlife to diseases and territorial conflicts, AWES technicians identify how an animal is getting in and seals points of entry without trapping the animal inside. The solution motivates the animal to leave the property and reduces the likelihood of other animals getting in.

Exclusion is ultimately more effective, more economical, and far more humane than most other services.
                          </p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter insects">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/silverfish.jpg');">
            <div class="card-category">INSECTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>Silverfish</strong></p>
              <p>Silverfish consume matter that contains polysaccharides, such as..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-silverfish" uk-toggle></a>
            <div id="modal-silverfish" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Silverfish</h2>
                          <p>Silverfish consume matter that contains polysaccharides, such as starches and dextrin in adhesives. These include book bindings, carpet, clothing, coffee, dandruff, glue, hair, some paints, paper, photos, plaster, and sugar. They will damage wallpaper in order to consume the paste. Silverfish can also cause damage to tapestries. Other substances they may eat include cotton, dead insects, linen, silk, or even their own exuvia (moulted exoskeleton). During famine, a silverfish may even attack leatherware and synthetic fabrics. Silverfish can live for a year or more without eating if water is available. Silverfish are considered household pests, due to their consumption and destruction of property.
                          </p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter insects">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/sowbug.jpg');">
            <div class="card-category">INSECTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>Sow Bugs</strong></p>
              <p>Sowbugs are land crustaceans which look very similar to pillbugs..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-sowbug" uk-toggle></a>
            <div id="modal-sowbug" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Sow Bugs</h2>
                          <p>Sowbugs are land crustaceans which look very similar to pillbugs, at least at first glance. Sowbugs are small crustaceans with oval bodies when viewed from above. Their back consists of a number of overlapping, articulating plates. They have 7 pairs of legs, and antennae which reach about half the body length. Most are slate gray in color, and may reach about 15 mm long and 8 mm wide.
<br>
Common names for woodlice vary throughout the English-speaking world. A number of common names make reference to the fact that some species of woodlice can roll up into a ball. Other names compare the woodlouse to a pig.
                          </p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter rodents">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/sparrows.jpg');">
            <div class="card-category">RODENTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>Sparrows</strong></p>
              <p>Sparrows damage crops and gardens by pecking seeds,..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-sparrows" uk-toggle></a>
            <div id="modal-sparrows" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Sparrows</h2>
                          <p>Sparrows damage crops and gardens by pecking seeds, seedlings, buds, flowers, vegetables and maturing fruits. They consume and spoil livestock food and water, and contaminate or deface buildings, facilities and livestock with their nests and droppings. They can also transmit many diseases and parasites of livestock, pets and humans.
                          </p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter insects">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/spider.jpg');">
            <div class="card-category">INSECTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>Spiders</strong></p>
              <p>The House Spider  are urban pests named after the comb-like row..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-spider" uk-toggle></a>
            <div id="modal-spider" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Spider</h2>
                          <p>The House Spider  are urban pests named after the comb-like row of bristles located on the tarsi of their fourth pair of legs. House spiders are common throughout the world, and their webs are most often found in corners, basements, crawlspaces, under furniture and around windows. The venom is necrotic, causing open, localized wounds that may be slow to heal.
                          </p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter rodents">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/squirel.jpg');">
            <div class="card-category">RODENTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>Richardson's ground squirrel</strong></p>
              <p>Because they will readily eat crop species, Richardson's ground squirrels..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-squirel" uk-toggle></a>
            <div id="modal-squirel" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Richardson's ground Squirrel</h2>
                          <p>Because they will readily eat crop species, Richardson's ground squirrels are sometimes considered to be agricultural pests, although this is not their legal status in all jurisdictions. The government of Saskatchewan declared the animals pests in 2010, allowing local governments to employ gopher control measures.
                          </p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter insects">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/tentcaterpillar.jpg');">
            <div class="card-category">INSECTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>Tent Caterpillars</strong></p>
              <p>Tent caterpillars are moderately sized caterpillars..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-tentcaterpillar" uk-toggle></a>
            <div id="modal-tentcaterpillar" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Tent Caterpillars</h2>
                          <p>Tent caterpillars are moderately sized caterpillars, or moth larvae, belonging to the genus Malacosoma in the family Lasiocampidae. Twenty-six species have been described, six of which occur in North America and the rest in Eurasia. Some species are considered to have subspecies as well. They are often considered pests due to their habit of defoliating trees. They are among the most social of all caterpillars and exhibit many noteworthy behaviors.
<br>Tent caterpillars are readily recognized because they are social, colorful, diurnal and build conspicuous silk tents in the branches of host trees. Some species, such as the eastern tent caterpillar, Malacosoma americanum, build a single large tent which is typically occupied through the whole of the larval stage, while others build a series of small tents that are sequentially abandoned. Whereas tent caterpillars make their tents in the nodes and branches of a tree's limbs, webworms enclose leaves and small branches at the ends of the limbs.
                          </p>

                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter wildlife">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/trapping.jpg');">
            <div class="card-category">WILDLIFE</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>Trapping</strong></p>
              <p>Traps are used as a method of pest control as an..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-trapping" uk-toggle></a>
            <div id="modal-trapping" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Trapping</h2>
                          <p>Traps are used as a method of pest control as an alternative to pesticides. Commonly spring traps which holds the animal are used — mousetraps for mice, or the larger rat traps for larger rodents like rats and squirrel. Specific traps are designed for invertebrates such as cockroaches and spiders. Some mousetraps can also double as an insect or universal trap, like the glue traps which catch any small animal that walks upon them.
                          </p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter wildlife">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/visualdeterrent.jpg');">
            <div class="card-category">WILDLIFE</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>Visual Deterrents</strong></p>
              <p>Visual bird deterrents are used to frighten birds from treated..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-visualdeterrent" uk-toggle></a>
            <div id="modal-visualdeterrent" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Visual Deterrents</h2>
                          <p>Visual bird deterrents are used to frighten birds from treated areas with common scare triggers such as predator features or reflective surfaces. The birds see the bird deterrent and flee the area. Visual bird deterrents are ideal to use in outdoor areas such as gardens, fruit trees, around pools, on patios and other areas birds like to hang out. Visual bird deterrents are commonly used in backyard and outdoor settings and are most effective when installed shortly after a "bird problem" is noticed. Where applicable, visual bird deterrents are an economic, low cost solution for many common outdoor pest bird problems.
                          </p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter rodents">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/voles.jpg');">
            <div class="card-category">RODENTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>Voles</strong></p>
              <p>You have heard of mole control, but not vole control? Why is that?..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-voles" uk-toggle></a>
            <div id="modal-voles" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Voles</h2>
                          <p>You have heard of mole control, but not vole control? Why is that? Well, the latter gets little recognition. While you may not know the difference between moles and voles, even those who are not landscaping enthusiasts have heard of moles. But most people go their whole lives without ever so much as hearing about voles, let alone controlling them. To make matters more confusing, these pests are sometimes referred to as "meadow mice" or "field mice." But when you identify the damage they cause in lawn and garden alike, you'll quickly learn that this is no "Mickey Mouse" problem.<br>
Voles construct well-defined, visible tunnels, or "runways" at or near the surface, about two inches wide. Vole runways result from the voles eating the grass blades, as well as from the constant traffic of numerous little feet beating the same path. And if any lawn and garden pest can literally “beat a path” through the grass due to their sheer numbers, it’s the voles. Rabbits don’t have anything over this prolific rodent!
                          </p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="gallery_product uk-padding-small col-lg-3 col-md-3 col-sm-12 col-xs-6 filter insects">
          <div class="card" style="background-image: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.7)), url(' img/controlservice/wasp.jpg');">
            <div class="card-category">INSECTS</div>
            <div class="card-description">
              <p class="text-left blue-darken-text"><strong>Wasps</strong></p>
              <p>The most commonly known wasps such as yellow jackets..<br><strong class="aqua-text">ReadMore</strong></p>
            </div>
            <a class="card-link" href="#modal-wasp" uk-toggle></a>
            <div id="modal-wasp" uk-modal="center: true" style="z-index: 99999;">
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-body">
                      <div class="row">
                        <div class="col-md-12">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <h2>Wasps</h2>
                          <p>The most commonly known wasps such as yellow jackets and hornets are in the Vespidae family and are eusocial, living together in a nest with an egg-laying queen and non-reproducing workers. Eusociality is favoured by the unusual haplodiploid system of sex determination in Hymenoptera, as it makes sisters exceptionally closely related to each other. However, the majority of wasp species are solitary, with each adult female living and breeding independently. Many of the solitary wasps are parasitoidal, meaning that they raise their young by laying eggs on or in the larvae of other insects. The wasp larvae eat the host larvae, eventually killing them. Solitary wasps parasitize almost every pest insect, making wasps valuable in horticulture for biological pest control of species such as whitefly in tomatoes and other crops
                          </p>

                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
</section>
